import {timeout} from './../config/helpers'

export default class Elevator {
    constructor(index, floor = 0) {
        this.id = 'elv-'+index
        this.floor = floor
        this.status = 'call'
        this.bell = new Audio('http://sfxcontent.s3.amazonaws.com/soundfx/ElevatorBell.mp3');
        this.time = 0
    }

    getPosition(x) {
        return {
            left: `calc(100% / ${window.state.elevators} * ${x+1} - 45px)`,
            bottom: `calc(100% / ${window.state.floors} * ${this.floor+1} - 30px)`
        }
    }

    getStyle(status) {
        switch(status) {
            case 'busy': return `black-red`
            case 'arrived': return `black-green`
            default: return ``
        }
    }

    setStatus(status) {
        this.status = status
    }

    setTime(time) {
        this.time = time
    }

    setTimeHtml() {
        if(this.time > 0)
            return Math.floor(this.time / 1000) + 'Sec'
    }

    getTransition(sec) {
        return {
            transition: `bottom ${sec}s ease`
        }
    }

    busy() {
        return this.status === 'busy'
    }

    async move(floor) {

        //set destination floor
        this.floor = floor.number
        // sleep 
        await timeout(window.state.queueTime)
        // set elevator to arrived
        this.setStatus('arrived')
        // clear time
        this.time = 0
        // play audio
        this.bell.play()
    }

}

