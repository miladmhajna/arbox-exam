export default class Floor {
    constructor(index) {
        this.name = this.setName(index),
        this.number = index
        this.status = 'call'
    }

    setName(index) {
        switch(index) {
            case 0: return 'Ground Floor'
            case 1: return `${index}st`
            case 2: return `${index}nd`
            case 3: return `${index}rd`
            default: return `${index}th`
        }
    }

    setStatus(value) {
        this.status = value
    }
}