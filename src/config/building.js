import Elevator from './../config/elevator'
import Floor from './../config/floor'

export default class Building {
    constructor(floorsCount, elevatorsCount) {
        this.floors = [];
        this.elevators = [];

        this.init(floorsCount, elevatorsCount)
    }

    init(floorsCount, elevatorsCount) {
        for(let i=floorsCount-1; i >= 0; i--) {
            this.floors.push(new Floor(i))
        }

        for(let i=0; i< elevatorsCount; i++) {
            this.elevators.push(new Elevator(i, i))
        }
    }

    getButtonStyle(status) {
        switch(status) {
            case 'waiting': return `is-danger`
            case 'arrived': return `is-success is-outlined`
            default: return `is-success`
        }
    }

    getButtonText(status) {
        switch(status) {
            case 'waiting': return `waiting`
            case 'arrived': return `arrived`
            default: return `call`
        }
    }

}

