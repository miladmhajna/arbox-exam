export default class Queue {
    constructor() {
        this.active = []
        this.inProgress = false
        this.length = 0
    }

    setActive(queue) {
        this.active = queue
    }

    setLength(length) {
        this.length = length
    }

    setInProgress(value) {
        this.inProgress = value
    }

    busy() {
        return this.inProgress
    }

}

