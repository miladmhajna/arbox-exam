import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

require('@/assets/main.scss');

window.state = {
  floors: 10,
  elevators: 5,
  queueTime: 1000
}

new Vue({
  render: h => h(App),
}).$mount('#app')
